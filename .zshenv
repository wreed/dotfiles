. "/etc/profile";

export WALLPAPERS=$HOME/Pictures/wallpapers;
export SCREENSHOTS=$HOME/Pictures/screenshots;
export SCRIPTS=$HOME/scripts;

export XDG_CONFIG_HOME=${XDG_CONFIG_HOME:="$HOME/.config"};
export XDG_CACHE_HOME=${XDG_CACHE_HOME:="$HOME/.cache"};
export XDG_DATA_HOME=${XDG_DATA_HOME:="$HOME/.local/share"};
export ZDOTDIR="$XDG_CONFIG_HOME/zsh";
export ZSH_PLUGIN_DIR="$ZDOTDIR/plugins";
export HISTFILE="$XDG_DATA_HOME/zsh/history";

. "$ZDOTDIR/functions.zsh"

# PATH
prepend_path PATH $HOME/.bin;
prepend_path PATH $HOME/.local/bin;
prepend_path PATH $HOME/.cargo/bin;
prepend_path PATH $HOME/.nimble/bin;
prepend_path PATH $HOME/.cabal/bin;
prepend_path PATH $HOME/.ghcup/bin;
prepend_path PATH $HOME/.linuxbrew/bin;

# default apps
export TERMINAL=kitty;
export EDITOR=nvim;
export GUIEDITOR=neovide;
export LAUNCHER=bemenu-run;
export BROWSER=firefox;
export MEDIAPLAYER=mpv;
export IMAGEVIEWER=nsxiv;
export FILEMANAGER=thunar;
export TUIFILEMANAGER=nnnp;
export COMPOSITOR=picom;

ln -sfT $(which $TERMINAL) $HOME/.bin/terminal;
ln -sfT $(which $EDITOR) $HOME/.bin/editor;
ln -sfT $(which $GUIEDITOR) $HOME/.bin/guieditor;
ln -sfT $(which $LAUNCHER) $HOME/.bin/launcher;
ln -sfT $(which $BROWSER) $HOME/.bin/browser;
ln -sfT $(which $MEDIAPLAYER) $HOME/.bin/mediaplayer;
ln -sfT $(which $IMAGEVIEWER) $HOME/.bin/imageviewer;
ln -sfT $(which $FILEMANAGER) $HOME/.bin/filemanager;
ln -sfT $(which $TUIFILEMANAGER) $HOME/.bin/tuifilemanager;
ln -sfT $(which $COMPOSITOR) $HOME/.bin/compositor;

# Source for other annoying/long env vars
. "$ZDOTDIR/env.zsh";
