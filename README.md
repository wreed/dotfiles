Will Reed's dotfiles
======

This is my Linux dotfile repository, where you can find nearly all of the
personal configuration files that I use on my Linux system daily.
In the future I plan to add a deployment script, but for now it is served
as is on the repository. If you have any questions, there is a contact
section at the bottom of this document.

Software Used
------
* ### Window Managers
  + [Awesome](https://github.com/awesomeWM/awesome "Awesome on GitHub") - Tiling WM forked from DWM with Lua extensibility
  + [River](https://github.com/riverwm/river "River on GitHub") - Tiling Wayland compositor written in Zig
  + [Hyprland](https://github.com/hyprwm/hyprland "Hyprland on GitHub") - Tiling Wayland compositor with cool animations
  + [XMonad](https://xmonad.org "XMonad website") - Tiling WM written in Haskell
  + [SpectrWM](https://github.com/conformal/spectrwm "Spectrwm on GitHub") - Tiling WM written in C with *easy* configuration
  + [QTile](https://qtile.org "QTile website") - Tiling WM written in Python
  + [LeftWM](https://leftwm.org "LeftWM website") - Tiling WM written in *rust btw*, configured in *TOML*
  + [HerbstluftWM](https://herbstluftwm.org "Herbstluftwm website") - Manual Tiling WM with *extensive* configurability
  + [Bspwm](https://github.com/baskerville/bspwm "Bspwm on GitHub") - Manual/pseudo-dynamic tiling WM that is *BiNaRy TrEe* or something
  + [Berry](https://berrywm.org/ "Berry website") - Manual tiling WM with configuration/control similar to Bspwm

* ### Terminals
  + [Kitty](https://sw.kovidgoyal.net/kitty/ "Kitty website") - *The terminal I use*
    * GPU Accelerated, Python plugin system *"kittens"*, ligature support and tons more
  + [Alacritty](https://alacritty.org "Alacritty website")
    * Cross-platform, *YAML* configured, GPU accelerated, written in *rust btw* 
  + [Foot](https://codeberg.org/dnkl/foot "Foot on Codeberg")
    * Wayland only, true color, can be run in *server* or *instance* mode 

* ### Status Bars
  + [XMobar](https://codeberg.org/xmobar/xmobar "XMobar on Codeberg")
    + Status bar configured/written in *Haskell* for use with XMonad and other X11 WM's
  + [Waybar](https://github.com/alexays/waybar "Waybar on GitHub")
    + Wayland status bar, configured in *JSON and CSS*
  + [Yambar](https://codeberg.org/dnkl/yambar "Yambar on Codeberg")
    + Wayland/X11 status bar configured in YAML, *works great with River*
  + [Polybar](https://polybar.github.io "Polybar website")
    + X11 status bar with tons of pre-made modules, configured with *ini* format

* ### Editor, CLI Tools, and other
  + [Powernim](https://codeberg.org/wreed/powernim "Powernim on Codeberg") ***written by yours truly***
    + Very basic power menu for linux (with systemd)
  + [Zsh](https://zsh.sourceforge.io/ "Zsh on SourceForge") - The shell I use
  + [NeoVim](https://github.com/neovim/neovim "NeoVim on GitHub") - The editor I use
  + [Glow](https://github.com/charmbracelet/glow "Glow on GitHub") - CLI markdown rendering
  + [Mako](https://wayland.emersion.fr/mako/ "Mako website") - Wayland notification daemon
  + [Dunst](https://dunst-project.org "Dunst project website") - X11/Wayland notification daemon
  + [Bat](https://github.com/sharkdp/bat "Bat on GitHub") - *cat* clone with extra functionality
  + [Swhkd](https://github.com/waycrate/swhkd "Swhkd on GitHub") - Wayland/X11/TTY compatible Sxhkd clone
  + [Exa](https://the.exa.website "Exa website") - *ls* clone with extra functionality
  + [Picom](https://github.com/yshui/picom "Picom on GitHub") - fork of compton > xcompmgr *(compositing for X11)*
  + [EWW](https://elkowar.github.io "EWW website") - X11/Wayland widgets in *Lisp/SCSS*
  + [fastfetch](https://github.com/LinusDierheimer/fastfetch "fastfetch on GitHub") - Fetch tool written in C, fast, very customizable
  + [Paru](https://github.com/morganamilo/paru "Paru on GitHub") - AUR Helper written in *rust btw*


Contact Me
======

 * ## [*Email Me*](mailto:wreedb@yandex.com "Email Me")
