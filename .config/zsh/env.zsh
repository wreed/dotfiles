#!/bin/zsh

# FIXES
export LESSHISTFILE=-;
export GIT_CONFIG_GLOBAL="$XDG_CONFIG_HOME/git/gitconfig";
export GIT_CONFIG_SYSTEM="$XDG_CONFIG_HOME/git/gitconfig";
export GOPATH="$XDG_DATA_HOME/go";
export GTK2_RC_FILES="$XDG_CONFIG_HOME/gtk-2.0/gtkrc";
export _JAVA_AWT_WM_NONREPARENTING=1

# ZSH PROMPT
export TYPEWRITTEN_PROMPT_LAYOUT="pure_verbose";
export TYPEWRITTEN_SYMBOL="->";
export TYPEWRITTEN_ARROW_SYMBOL="-->";
export TYPEWRITTEN_RELATIVE_PATH="adaptive";
export TYPEWRITTEN_CURSOR="terminal";
export TYPEWRITTEN_COLORS="prompt:default;user:blue;host:green;symbol:cyan;current_directory:green";

# NNN
export NNN_FIFO="/tmp/nnn.fifo";
export NNN_TMPFILE="/tmp/nnn.lastd";
export NNN_TRASH=1;
export NNN_PLUG='p:preview-tui;x:!chmod +x $nnn';

# export BEMENU_OPTS="-l 10 -p 'run: ' -w -i --fn 'Play 15' -c -W 0.2 -B 2 --scrollbar never --bdr '#555753' --scf '#6AA1FD' --hb '#6AA1FD' --cf '#27293500' --hf '#272935' --tb '#272935' --tf '#EEEEEC' --fork"
export BEMENU_OPTS="bemenu-run --prompt 'run:' --fn 'Play 17' --list 8 --wrap --index 40 --line-height 17 --grab --ignorecase --width-factor 100 --ifne --monitor focused --hf '#181921' --hb '#6AA1FD' --tf '#181921' --tb '#6AA1FD' --cf '#6AA1FD' --nb '#181921' --nf '#D3D7CF' --ab '#181921' --af '#D3D7CF' --ff '#EEEEEC' --fb '#000000'";

# FZF
export FZF_DEFAULT_OPTS="-i \
  +x \
  --algo=v1 \
  --no-hscroll \
  --layout=reverse \
  --border=sharp \
  --margin='5%,10%,5%,10%' \
  --info=hidden \
  --pointer '>' \
  --prompt='  ' \
  --marker='(' \
  --header='FZF'
  --color='16' \
  ";
