#!/usr/bin/zsh

alias ff=fastfetch;
alias pf=pfetch;
alias nf=neofetch;
alias l='exa --icons -alhF --octal-permissions --no-permissions --no-time --group-directories-first';
alias ll='exa --icons -alhF --octal-permissions --no-permissions --time modified --group-directories-first';
alias la='exa --icons -aF --group-directories-first';
alias m=micro;
alias mkdir='mkdir -pv';
alias cp='cp -iv';
alias mv='mv -iv';
alias rm='rm -Iv';
alias nv=nvim;
alias pac="sudo pacman";
alias nnn="nnn -P p";
alias envv="env | sort";
alias baregit="git --git-dir=$HOME/dotfiles/ --work-tree=$HOME";

alias aura="sudo aura";
alias ssctl="sudo systemctl";
