#!/bin/zsh

remove_from_path() {
  local a
  local p
  local s
  local r
  eval "p=\$$1"  # get value of specified path
  a=( ${(s/:/)p} )  # turn it into an array
  # return if $2 isn't in path
  if [[ ${a[(i)${2}]} -gt ${#a} ]] && return
  # rebuild path from elements not matching $2
  for s in $a; do
    if [[ ! $s == $2 ]]; then
      [[ -z "$r" ]] && r=$s || r="$r:$s"
    fi
  done
  eval $1="$r"
}

prepend_path() {
  [[ ! -d "$2" ]] && return
  local p
  remove_from_path "$1" "$2"
  eval "p=\$$1"
  eval export $1="$2:$p"
}

append_path() {
  [[ ! -d "$2" ]] && return
  local p
  remove_from_path "$1" "$2"
  eval "p=\$$1"
  eval export $1="$p:$2"
}

fontq() { fc-list : family | rg -i $1 }

readme() { glow -p README.md }

mktar() {
  if [ -z "$1" ]
  then
    printf "no argument given...\n";
  else
    archive=$(echo "$1" | perl -pe 's|\/||g')
    bsdtar -cvf $archive.tar $archive; gzip -9 $archive.tar
  fi
}

untar() {
  if [ -z "$1" ]
  then
    printf "no argument given...\n";
  else
    bsdtar -xvf $1
  fi
}

wmdir() {
  cd $(printf "$XDG_CONFIG_HOME/$DESKTOP_SESSION\n")
}

man() {
	env \
		LESS_TERMCAP_md=$(tput bold; tput setaf 4) \
		LESS_TERMCAP_me=$(tput sgr0) \
		LESS_TERMCAP_mb=$(tput blink) \
		LESS_TERMCAP_us=$(tput setaf 2) \
		LESS_TERMCAP_ue=$(tput sgr0) \
		LESS_TERMCAP_so=$(tput smso) \
		LESS_TERMCAP_se=$(tput rmso) \
		PAGER="${commands[less]:-$PAGER}" \
		man "$@"
}

db() { sudo updatedb; sudo fc-cache -f; sudo mandb --quiet; fc-cache -f}

zd() {
  cd $(fd . "$PWD" -d 1 -t d -H | perl -pe "s|$PWD\/||g; s|\/$||g;" | fzf);
}

whichlink() {
  which $1 | xargs -r readlink;
}

whichcat() {
  which $1 | xargs -r readlink | xargs bat;
}
