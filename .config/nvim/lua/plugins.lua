vim.cmd [[packadd packer.nvim]];

local packer = require "packer";

packer.startup(function(use)

  use "wbthomason/packer.nvim";
  use "vim-airline/vim-airline";
  use "vim-airline/vim-airline-themes";
  use "romgrk/doom-one.vim";
  use "joshdick/onedark.vim";
  use "tomasiser/vim-code-dark";

  use "goolord/haskell-nvim";
  use "waycrate/swhkd-vim";
  use "alaviss/nim.nvim";

  use "majutsushi/tagbar";
  use "scrooloose/nerdtree";
  use "xuyuanp/nerdtree-git-plugin";
  use "ryanoasis/vim-devicons";
  use "airblade/vim-gitgutter";
  use "tpope/vim-commentary";
  use { "neoclide/coc.nvim", branch = "release" }

end)
