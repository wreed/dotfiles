-- vim:filetype=lua:expandtab:shiftwidth=2:tabstop=4:softtabstop=2:textwidth=100
-- Will Reed | wreedb@yandex.com | https://github.com/wreedb/wbr-linux
----------------------------------------------------------------------
-- AwesomeWM Configuration | AUTOSTART |

local awful = require "awful"
local naughty = require "naughty"

require "wbr.variables"
require "cfg.widgets"

awful.spawn.single_instance("wmname awesome")
awful.spawn.single_instance("numlockx")
awful.spawn.single_instance("unclutter --timeout 5 --jitter 50")
awful.spawn.single_instance("lxsession")

if not awesome.composite_manager_running then
  awful.spawn.single_instance("picom --daemon --config " .. xdg_configdir .. "picom/awesome.conf");
end

awful.spawn.easy_async_with_shell(pacman_cmd, function()
  awful.spawn.easy_async_with_shell(pacman_callback, function(out) 
    if (out == 0) then
      updates.markup = "Up to date";
    else
      updates.markup = "Updates: " .. out;
    end
  end)
end)