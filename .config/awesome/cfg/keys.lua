-- vim:filetype=lua:expandtab:shiftwidth=2:tabstop=4:softtabstop=2:textwidth=100
-- Will Reed | wreedb@yandex.com | https://github.com/wreedb/wbr-linux
----------------------------------------------------------------------
-- AwesomeWM Configuration | KEYBINDS |

local awful = require "awful"
local gears = require "gears"
local menubar = require "menubar"

local hotkeys_popup = require "awful.hotkeys_popup"

require "wbr.variables"

-- {{{ Mouse bindings

awful.mouse.append_global_mousebindings({ awful.button({ }, 3, function () mymainmenu:toggle() end) })

-- {{{ Key bindings

-- General Awesome keys
awful.keyboard.append_global_keybindings({
  
  awful.key({ modkey }, "/",
    hotkeys_popup.show_help,
  {description = "show help", group = "awesome"}),

  awful.key({ modkey, "Shift" }, "r", function()
    awful.util.restart() end,
  {description = "reload awesome", group = "awesome"}),

  awful.key({ modkey, "Shift" }, "q", function()
    awesome.quit() end,
  {description = "quit awesome", group = "awesome"}),
  
  awful.key({ modkey }, "Return",
    function() awful.spawn(terminal) end,
  {description = "open a terminal", group = "launcher"}),
  
  awful.key({ modkey }, "r",
    function()
      toggle_promptbox(true)
      awful.prompt.run {
        prompt        = "run: ",
        textbox       = awful.screen.focused().promptbox.widget,
        done_callback = function() toggle_promptbox(false) end,
      }
    end,
  {description = "run prompt", group = "launcher"}),
  
  awful.key({ modkey }, "p",
    function() awful.spawn(launcher) end,
  {description = "run launcher", group = "launcher"}),

  awful.key({ modkey, "Shift" }, "p",
    function() menubar.show() end,
  {description = "show menubar", group = "launcher"}),
  
  awful.key({ modkey }, "x",
    function() awful.spawn("powernim") end,
  {description = "power menu", group = "launcher"}),

})

-- Tags related keybindings
awful.keyboard.append_global_keybindings({
  awful.key({ modkey }, "Left",   awful.tag.viewprev,        { description = "view previous", group = "tag" }),
  awful.key({ modkey }, "Right",  awful.tag.viewnext,        { description = "view next",     group = "tag" }),
})

-- Function keys
awful.keyboard.append_global_keybindings({

  awful.key({ }, "XF86AudioRaiseVolume",
    function() awful.spawn("volume-up") end,
  {description = "increase volume", group = "function keys" }),

  awful.key({ }, "XF86AudioLowerVolume",
    function() awful.spawn("volume-down") end,
  {description = "lower volume", group = "function keys" }),

  awful.key({ }, "XF86AudioMute",
    function() awful.spawn("volume-mute") end,
  {description = "mute volume", group = "function keys" }),

  awful.key({ }, "XF86MonBrightnessUp",
    function() awful.spawn("brightness-up") end,
  {description = "increase brightness", group = "function keys" }),

  awful.key({ }, "XF86MonBrightnessDown",
    function() awful.spawn("brightness-down") end,
  {description = "lower brightness", group = "function keys" }),

  awful.key({ }, "Print",
    function() awful.spawn("screenshot") end,
  {description = "take a screenshot", group = "function keys" }),

  awful.key({ modkey }, "Print",
    function() awful.spawn("screenshot --select") end,
  {description = "take a selective screenshot", group = "function keys" }),

  awful.key({ modkey, "Mod1" }, "Print",
    function() awful.spawn("screenshot --wait 5") end,
  {description = "screenshot in 5 seconds", group = "function keys" }),

})

-- Focus related keybindings
awful.keyboard.append_global_keybindings({

  awful.key({ modkey }, "j",
    function() awful.client.focus.byidx(1) end,
  {description = "focus next by index", group = "client"}),

  awful.key({ modkey }, "k",
    function() awful.client.focus.byidx(-1) end,
  {description = "focus previous by index", group = "client"}),
  
  awful.key({ modkey }, "Tab",
    function() awful.client.focus.history.previous()
      if client.focus then client.focus:raise() end
    end,
  {description = "go back", group = "client"}),
    
  awful.key({ modkey, "Control" }, "j",
    function() awful.screen.focus_relative(1) end,
  {description = "focus the next screen", group = "screen"}),

  awful.key({ modkey, "Control" }, "k",
    function() awful.screen.focus_relative(-1) end,
  {description = "focus the previous screen", group = "screen"}),
  
  awful.key({ modkey, "Control" }, "n",
    function ()
      local c = awful.client.restore() -- Focus restored client
      if c then
        c:activate { raise = true, context = "key.unminimize" }
      end
    end,
  {description = "restore minimized", group = "client"}),

})

-- Layout related keybindings
awful.keyboard.append_global_keybindings({
    
  awful.key({ modkey, "Shift" }, "j",
    function() awful.client.swap.byidx(1) end,
  {description = "swap with next client by index", group = "client"}),
  
  awful.key({ modkey, "Shift" }, "k",
    function() awful.client.swap.byidx(-1) end,
  {description = "swap with previous client by index", group = "client"}),
  
  awful.key({ modkey }, "u",
    awful.client.urgent.jumpto,
  {description = "jump to urgent client", group = "client"}),

  awful.key({ modkey }, "l",
    function() awful.tag.incmwfact(0.05) end,
  {description = "increase master width factor", group = "layout"}),
  
  awful.key({ modkey }, "h",
    function() awful.tag.incmwfact(-0.05) end,
  {description = "decrease master width factor", group = "layout"}),
  
  awful.key({ modkey, "Shift" }, "h",
    function() awful.tag.incnmaster( 1, nil, true) end,
  {description = "increase the number of master clients", group = "layout"}),
  
  awful.key({ modkey, "Shift"   }, "l",
    function() awful.tag.incnmaster(-1, nil, true) end,
  {description = "decrease the number of master clients", group = "layout"}),
  
  awful.key({ modkey, "Control" }, "h",
    function() awful.tag.incncol(1, nil, true) end,
  {description = "increase the number of columns", group = "layout"}),
  
  awful.key({ modkey, "Control" }, "l",
    function() awful.tag.incncol(-1, nil, true) end,
  {description = "decrease the number of columns", group = "layout"}),
  
  awful.key({ modkey }, "space",
    function () awful.layout.inc(1) end,
  {description = "select next", group = "layout"}),
  
  awful.key({ modkey, "Shift" }, "space",
    function() awful.layout.inc(-1) end,
  {description = "select previous", group = "layout"}),

})


awful.keyboard.append_global_keybindings({
  awful.key {
    modifiers   = { modkey },
    keygroup    = "numrow",
    description = "only view tag",
    group       = "tag",
    on_press    = function (index)
      local screen = awful.screen.focused()
      local tag = screen.tags[index]
      if tag then
        tag:view_only()
      end
    end,
  },
  awful.key {
    modifiers = { modkey, "Shift" },
    keygroup    = "numrow",
    description = "move focused client to tag",
    group       = "tag",
    on_press    = function (index)
      if client.focus then
        local tag = client.focus.screen.tags[index]
        if tag then
          client.focus:move_to_tag(tag)
        end
      end
    end,
  },
})

client.connect_signal("request::default_mousebindings", function()
  awful.mouse.append_client_mousebindings({
    awful.button({},         1, function(c) c:activate {context = "mouse_click"} end),
    awful.button({ modkey }, 1, function(c) c:activate {context = "mouse_click", action = "mouse_move"} end),
    awful.button({ modkey }, 3, function(c) c:activate {context = "mouse_click", action = "mouse_resize"} end),
  })
end)

client.connect_signal("request::default_keybindings", function()
  awful.keyboard.append_client_keybindings({
    
    awful.key({ modkey, "Shift" }, "f",
      function (c)
        c.fullscreen = not c.fullscreen
        c:raise()
      end,
    {description = "toggle fullscreen", group = "client"}),
    
    awful.key({ modkey, "Shift" }, "c",
      function(c) c:kill() end,
    {description = "close", group = "client"}),
    
    awful.key({ modkey }, "f",
      awful.client.floating.toggle,
    {description = "toggle floating", group = "client"}),
    
    -- The client currently has the input focus, so it cannot be   
    -- minimized, since minimized clients can't have the focus.
    awful.key({ modkey }, "m",
      function (c) c.minimized = true end,
    {description = "minimize", group = "client"}),
  
  })
end)
