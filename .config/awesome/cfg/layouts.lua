-- vim:filetype=lua:expandtab:shiftwidth=2:tabstop=4:softtabstop=2:textwidth=100
-- Will Reed | wreedb@yandex.com | https://github.com/wreedb/wbr-linux
----------------------------------------------------------------------
-- AwesomeWM Configuration | LAYOUTS |

local awful = require "awful"
local lay = awful.layout.suit

tag.connect_signal("request::default_layouts", function() awful.layout.append_default_layouts({
  lay.tile,
  lay.floating,
  lay.max.fullscreen
})end)

-- AVAILABLE LAYOUTS:
--
-- tile(.[left/bottom/top]) | fair(.horizontal) | spiral(.dwindle) | magnifier | max(.fullscreen) | corner.[nw/ne/sw/se]