-- vim:filetype=lua:expandtab:shiftwidth=2:tabstop=4:softtabstop=2:textwidth=100
-- Will Reed | wreedb@yandex.com | https://github.com/wreedb/wbr-linux
----------------------------------------------------------------------
-- AwesomeWM Configuration | NOTIFICATIONS |

local ruled   = require "ruled"
local awful   = require "awful"
local naughty = require "naughty"
local gears   = require "gears"
local wibox   = require "wibox"
local pretty  = require "beautiful"
local colors  = require "wbr.pallette"


ruled.notification.connect_signal('request::rules', function()
  -- All notifications will match this rule.
  ruled.notification.append_rule {
    rule       = {},
    properties = {
      screen = awful.screen.preferred,
      implicit_timeout = 4,
      width = 400,
      font = "Play 16",
      border_width = 2,
      border_color = colors.blue,
    }
  }
end)

naughty.connect_signal("request::display", function(n)
  naughty.layout.box { notification = n }
end)
