-- vim:ft=haskell

Config { overrideRedirect = False
       , font       = "xft:Play:size=14:antialias=true"
       , bgColor    = "#272935"
       , fgColor    = "#EEEEEC"
       , position   = TopH 32
       , textOffset = 22
       , 
       , commands   = 
         [ Run Weather "KCIU"
           [ "--template", "<weather> <tempF>°F"
           , "-L", "40"
           , "-H", "80"
           , "--low"   , "#6DE4FD"
           , "--normal", "#6BF1BE"
           , "--high"  , "#F47375"
           ] 36000
         , Run Cpu
           [ "-L", "3"
           , "-H", "50"
           , "--high"  , "red"
           , "--normal", "green"
           ] 10
         , Run Memory ["--template", "Mem: <usedratio>%"] 10
         , Run Date "<fc=#8be9fd>%I:%M</fc> %p " "date" 10
         , Run UnsafeXMonadLog
         ]
       , sepChar  = "%"
       , alignSep = "}{"
       , template = " <action=`bemenu-run`>Menu</action> | %UnsafeXMonadLog%}{%cpu% | <box type=Bottom width=2> %memory% </box> | %KCIU% | %date%"
       }

