#!/bin/sh

clock() {
  DATETIME=$(date "+%I:%M %p");
  printf "$DATETIME";
}

while true; do
  echo "%{r}%{F#1E1F29}%{B#6AA1FD} $(clock) %{F-}%{B-}"
  sleep 1
done
