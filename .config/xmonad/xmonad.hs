-- vim:ft=haskell
import XMonad
import XMonad.ManageHook
import XMonad.Layout.Spacing

-- Utils
import XMonad.Util.EZConfig (additionalKeysP)
import XMonad.Util.Loggers
import XMonad.Util.SpawnOnce (spawnOnce)
import XMonad.Util.ClickableWorkspaces

-- Hooks
import XMonad.Hooks.SetWMName (setWMName)
import XMonad.Hooks.EwmhDesktops (ewmh,ewmhFullscreen)
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.StatusBar
import XMonad.Hooks.StatusBar.PP
import XMonad.Hooks.WindowSwallowing (swallowEventHook)
import XMonad.Hooks.ManageHelpers (doCenterFloat)

-- Haskell libs
import qualified Data.Map as M
import Data.Maybe (fromJust)

-- Main
main :: IO ()
main = xmonad
     . withSB bar
     . ewmhFullscreen
     . ewmh
     . docks
     $ cfg

-- XMobar Clickable Workspaces
wspaces   = [" 1 ", " 2 ", " 3 ", " 4 ", " 5 ", " 6 ", " 7 ", " 8 ", " 9 "]
wspaceidx = M.fromList $ zipWith (,) wspaces [1..]

clickable ws = "<action=xdotool key super+"++show i++">"++ws++"</action>"
  where i = fromJust $ M.lookup ws wspaceidx

-- Personal Config
cfg = def
  { manageHook          = managehook
  , handleEventHook     = swallowEventHook (className =? "kitty" <||> className =? "Alacritty") (return True)
  , borderWidth         = 2
  , focusFollowsMouse   = True
  , modMask             = mod4Mask
  , workspaces          = wspaces
  , layoutHook          = layouts
  , startupHook         = autostart
  , terminal            = "terminal"
  , normalBorderColor   = "#464A5F"
  , focusedBorderColor  = "#6AA1FD"
  }
  `additionalKeysP`
  [ ("M-S-c"      , kill                   )
  , ("M-S-q"      , spawn "powernim"       )
  , ("M-x"        , spawn "powermonad"     )
  , ("M-S-r"      , spawn "xmonad-restart" )
  , ("M-S-e"      , spawn "editwmconfig"   )
  , ("M-e"        , spawn "gtk-launch guieditor.desktop" )
  , ("M-<Return>" , spawn "gtk-launch terminal.desktop"  )
  , ("M-p"        , spawn "gtk-launch launcher.desktop"  )
  , ("M-S-b"      , spawn "gtk-launch browser.desktop"   )

  , ("M-S-g"                   , spawn "xfce4-appearance-settings" )
  , ("M-S-<Esc>"               , spawn "betterlockscreen -l"       )
  , ("<Print>"                 , spawn "screenshot"                )
  , ("M-<Print>"               , spawn "screenshot --select"       )
  , ("<XF86MonBrightnessUp>"   , spawn "brightness --up"           )
  , ("<XF86MonBrightnessDown>" , spawn "brightness --down"         )
  , ("<XF86AudioMute>"         , spawn "volume --mute"             )
  , ("<XF86AudioRaiseVolume>"  , spawn "volume --up"               )
  , ("<XF86AudioLowerVolume>"  , spawn "volume --down"             )
  ]

layouts = space $ avoidStruts tiled ||| Mirror tiled ||| Full
  where
    tiled   = Tall nmaster delta ratio
    nmaster = 1
    ratio   = 1/2
    delta   = 3/100

space =
  spacingRaw False     -- False=Apply even when single window
  (Border 7 7 7 7)     -- Screen border size top bot rght lft
  True                 -- Enable screen border
  (Border 7 7 7 7)     -- Window border size
  True                 -- Enable window borders

autostart = do
  spawnOnce "picom"
  spawnOnce "hsetroot -fill ~/Pictures/wallpapers/girl.png"
  spawnOnce "lxpolkit"
  spawnOnce "xfsettingsd"
  spawnOnce "dunst"
  setWMName "xmonad"

managehook = composeAll
  [ className =? "Powernim"                  --> doCenterFloat
  , className =? "Powermonad"                --> doCenterFloat
  , className =? "Pavucontrol"               --> doCenterFloat
  , className =? "Xfce4-appearance-settings" --> doCenterFloat
  , title     =? "Open Folder"               --> doCenterFloat
  , title     =? "Open File"                 --> doCenterFloat
  ]

bar = statusBarProp "xmobar" (pure myXmobarPP)

myXmobarPP :: PP
myXmobarPP = def
    { ppSep             = magenta " | "
    , ppTitleSanitize   = xmobarStrip
    , ppCurrent         = wrap " " "" . xmobarBorder "Top" "#6AA1FD" 32
    , ppVisible         = wrap "" "" . clickable
    , ppHidden          = white . wrap " " "" . clickable
    , ppHiddenNoWindows = lowWhite . wrap " " "" . clickable
    , ppUrgent          = red . wrap (yellow "!") (yellow "!")
    , ppOrder           = \[ws, l, _, wins] -> [ws, l, wins]
    , ppExtras          = [logTitles formatFocused formatUnfocused]
    }
  where
    formatFocused   = wrap (white    " [ ") (white    " ] ") . green . ppWindow
    formatUnfocused = wrap (lowWhite " [ ") (lowWhite " ] ") . white . ppWindow

    -- | Windows should have *some* title, which should not not exceed a
    -- sane length.
    ppWindow :: String -> String
    ppWindow = xmobarRaw . (\w -> if null w then "untitled" else w) . shorten 30

    blue, green, lowWhite, magenta, red, white, yellow :: String -> String
    magenta  = xmobarColor "#C481FF" ""
    green    = xmobarColor "#6BF1BE" ""
    blue     = xmobarColor "#6AA1FD" ""
    white    = xmobarColor "#EEEEEC" ""
    yellow   = xmobarColor "#E5F081" ""
    red      = xmobarColor "#F47375" ""
    lowWhite = xmobarColor "#D3D7CF" ""
