#!/usr/bin/luajit

confighome = os.getenv("XDG_CONFIG_HOME")
wmname     = os.getenv("DESKTOP_SESSION")
compositor = os.getenv("COMPOSITOR")

unclutteropts  = " --jitter 50 --timeout 4 --fork"
compositoropts = " --daemon --config "..confighome.."/"..compositor.."/"..wmname..".conf"

os.execute("numlockx")
os.execute("unclutter"..unclutteropts)
os.execute(compositor..compositoropts)
os.execute("wallpaper")
os.execute("lxpolkit &>/dev/null &")
os.execute("dunst &>/dev/null &")
