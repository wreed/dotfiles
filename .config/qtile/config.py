import os
import re
import socket
import subprocess
from libqtile import bar, layout, widget, hook
from libqtile.config import Click, Drag, Group, Key, Match, Screen
from libqtile.lazy import lazy
from typing import List

mod = "mod4"
terminal = "terminal"

keys = [
  Key([mod], "l",
    lazy.layout.shrink(),
    desc="Grow window to the right"),
  
  Key([mod], "h",
    lazy.layout.grow(),
    desc="Grow window to the left"),
  
  Key([mod], "j",
    lazy.layout.next(),
    desc="Move focus down"),
  
  Key([mod], "k",
    lazy.layout.previous(),
    desc="Move focus up"),
  
  Key([mod], "Tab",
    lazy.layout.next(),
    desc="Move window focus to other window"),
  
  Key([mod, "shift"], "h",
    lazy.layout.shuffle_left(),
    desc="Move window to the left"),
  
  Key([mod, "shift"], "l",
    lazy.layout.shuffle_right(),
    desc="Move window to the right"),
 
  Key([mod, "shift"], "j",
    lazy.layout.shuffle_down(),
    desc="Move window down"),
 
  Key([mod, "shift"], "k",
    lazy.layout.shuffle_up(),
    desc="Move window up"),
  
  Key([mod], "n",
    lazy.layout.normalize(),
    desc="Reset all window sizes"),

  Key([mod, "shift"], "Return",
    lazy.layout.toggle_split(),
    desc="Toggle between split and unsplit sides of stack"),
  
  Key([mod],"Return",     
    lazy.spawn("terminal"),
    desc="Launch terminal"),
  
  Key([mod],"p",
    lazy.spawn("launcher"),
    desc="Run launcher"),
  
  Key([mod,"shift"],"b",
    lazy.spawn("browser"),
    desc="Launch browser"),
  
  Key([mod],"e",
    lazy.spawn("editwmconfig"),
    desc="Edit config.py"),

  Key([mod],"space",
    lazy.next_layout(),
    desc="Toggle between layouts"),
  
  Key([mod,"shift"],"c",  
    lazy.window.kill(),
    desc="Kill focused window"),
  
  Key([mod,"shift"],"r",
    lazy.reload_config(),
    desc="Reload the config"),
  
  Key([mod,"shift"],"q",
    lazy.shutdown(),
    desc="Shutdown Qtile"),

  Key([], "XF86AudioRaiseVolume",
    lazy.spawn("volume-up"),
    desc = "Raise volume"),
  
  Key([], "XF86AudioLowerVolume",
    lazy.spawn("volume-down"),
    desc = "Lower volume"),

  Key([], "XF86AudioMute",
    lazy.spawn("volume-mute"),
    desc = "Mute volume"),

  Key([], "XF86MonBrightnessUp",
    lazy.spawn("brightness-up"),
    desc = "Increase brightness"),
  
  Key([], "XF86MonBrightnessDown",
    lazy.spawn("brightness-down"),
    desc = "Decrease brightness"),
  
  Key([], "Print",
    lazy.spawn("screenshot-xorg"),
    desc = "Take a screenshot"),

  Key([mod], "Print",
    lazy.spawn("screenshot-select-xorg"),
    desc = "Take a selection screenshot"),
    
  Key([mod], "x",
    lazy.spawn("powermenu"),
    desc = "Launch the powermenu"),

]

groups = [Group(n) for n in "123456789" ]

for n in groups:
  keys.extend(
    [
      Key([mod],n.name,
        lazy.group[n.name].toscreen(),
        desc="Focus workspace {}".format(n.name),),
      Key([mod,"shift"],n.name,
        lazy.window.togroup(n.name, switch_group=True),
        desc="Send to workspace {}".format(n.name),),
    ]
  )


layout_theme = {
  "border_width":  2,
  "margin":        10,
  "border_focus":  "6DE4FD",
  "border_normal": "000000",
}
floating_theme = {
  "border_width": 2,
  "border_focus":  "6DE4FD",
  "border_normal": "000000",
}

layouts = [
  layout.MonadTall(**layout_theme),  # MonadWide(),RatioTile(),Tile(),
  layout.Max(**layout_theme),        # TreeTab(),VerticalTile(),Zoomy(),
  layout.Floating(**floating_theme),
]

colors = {
  "bg":      "#272935",
  "fg":      "#EEEEEC",
  "red":     "#F47375",
  "green":   "#6BF1BE",
  "yellow":  "#E5F081",
  "blue":    "#6AA1FD",
  "magenta": "#C481FF",
  "cyan":    "#6DE4FD",
  "white":   "#D3D7CF",
  "grey":    "#555753",
}

widget_defaults = dict(
  font = "Play Nerd Font",
  fontsize = 18,
  padding = 0,
  background = colors["bg"],
  foreground = colors["fg"],
)

extension_defaults = widget_defaults.copy()
# 
screens = [
  Screen(
    top=bar.Bar(
      [ 
        widget.Sep(
          linewidth = 0,
          padding = 6,
          foreground = colors["fg"],
          background = colors["bg"],
        ),
        widget.Image(
          filename = "~/.config/qtile/icons/logo.svg", 
          margin = 5,
          scale = True,
        ),
        widget.Sep(
          linewidth = 0,
          padding = 6,
          foreground = colors["fg"],
          background = colors["bg"],
        ),
        widget.GroupBox(
          highlight_method = "line",
          highlight_color  = [colors["blue"],colors["blue"]],
          padding = 8,
        ),
        widget.Sep(linewidth = 0,padding = 6,foreground = colors["fg"],background = colors["bg"]),
        widget.Spacer(),
        widget.TextBox(
          text = "",
          foreground = colors["magenta"],
          background = colors["bg"],
          font = "Symbols Nerd Font",
          fontsize = 50,
          padding = 0,
        ),
        widget.CheckUpdates(
          fmt = "  {}",
          foreground = "#272935",
          background = colors["magenta"],
        ),
        widget.TextBox(
          text = "",
          foreground = colors["yellow"],
          background = colors["magenta"],
          font = "Symbols Nerd Font",
          fontsize = 50,
          padding = 0,
        ),
        widget.TextBox(
          foreground = colors["bg"],
          background = colors["yellow"],
          text = "  ﯦ",
        ),
        widget.Backlight(
          backlight_name = "amdgpu_bl0",
          change_command = "light -S {0}",
          step = 5,
          fmt = "{}",
          foreground = colors["bg"],
          background = colors["yellow"],
        ),
        widget.TextBox(
          text = "",
          foreground = colors["cyan"],
          background = colors["yellow"],
          font = "Symbols Nerd Font",
          fontsize = 50,
          padding = 0,
        ),
        widget.PulseVolume(
          get_volume_command = "pamixer --get-volume",
          limit_max_volume = True,
          volume_app = "pamixer",
          volume_down_command = "-d 1",
          volume_up_command = "-i 1",
          mute_command = "-t",
          fmt = "  {}",
          foreground = colors["bg"],
          background = colors["cyan"],
          padding = 0,
        ),
        widget.TextBox(
          text = "",
          foreground = colors["red"],
          background = colors["cyan"],
          font = "Symbols Nerd Font",
          fontsize = 50,
          padding = 0,
        ),
        widget.Memory(
          measure_mem = "G",
          fmt = "   {}",
          foreground = colors["bg"],
          background = colors["red"],
          padding = 0,
        ),
        widget.TextBox(
          text = "",
          foreground = colors["blue"],
          background = colors["red"],
          font = "Symbols Nerd Font",
          fontsize = 50,
          padding = 0,
        ),
        widget.CPU(
          format = "{load_percent}%",
          fmt = "   {}",
          background = colors["blue"],
          foreground = colors["bg"],
          padding = 0,
        ),
        widget.TextBox(
          text = "",
          foreground = colors["green"],
          background = colors["blue"],
          font = "Symbols Nerd Font",
          fontsize = 50,
          padding = 0,
        ),
        widget.Clock(
          format = "%I:%M %p",
          fmt = "  {}",
          font = "Play",
          background = colors["green"],
          foreground = colors["bg"],
          marging = 0,
        ),
        widget.TextBox(
          text = "",
          foreground = colors["magenta"],
          background = colors["green"],
          font = "Symbols Nerd Font",
          fontsize = 50,
          padding = 0,
        ),
        widget.CurrentLayoutIcon(
          custom_icon_paths = [os.path.expanduser("~/.config/qtile/icons")],
          foreground = colors["bg"],
          background = colors["magenta"],
          padding = 2,
          scale = 0.7
        ),
        widget.Sep(linewidth = 0,padding = 6,foreground = colors["magenta"],background = colors["magenta"]),
      ],
      32,
      border_width=[2, 0, 2, 0],  # Draw top and bottom borders
      border_color=["000000", "000000", "000000", "000000"]  # Borders are magenta
    ),
  ),
]

# Drag floating layouts.
mouse = [
  Drag([mod], "Button1", lazy.window.set_position_floating(), start=lazy.window.get_position()),
  Drag([mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()),
  Click([mod], "Button2", lazy.window.bring_to_front()),
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: list
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(
  float_rules=[
    *layout.Floating.default_float_rules, # Run the utility of `xprop` to see the wm class and name of an X client.
    Match(wm_class="confirmreset"),       # gitk
    Match(wm_class="makebranch"),         # gitk
    Match(wm_class="maketag"),            # gitk
    Match(wm_class="ssh-askpass"),        # ssh-askpass
    Match(title="branchdialog"),          # gitk
    Match(title="pinentry"),              # GPG key password entry
  ]
)

auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True
auto_minimize = True
wl_input_rules = None

@hook.subscribe.startup_once
def autostart():
  processes = [["autostart.lua"]]
  for n in processes:
    subprocess.Popen(n)

wmname = "qtile"
