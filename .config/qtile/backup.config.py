import os
import re
import socket
import subprocess
from libqtile import bar, layout, widget, hook
from libqtile.config import Click, Drag, Group, Key, Match, Screen
from libqtile.lazy import lazy
from typing import List

mod = "mod4"
terminal = "terminal"

keys = [
  Key([mod], "l",
    lazy.layout.shrink(),
    desc="Grow window to the right"),
  
  Key([mod], "h",
    lazy.layout.grow(),
    desc="Grow window to the left"),
  
  Key([mod], "j",
    lazy.layout.next(),
    desc="Move focus down"),
  
  Key([mod], "k",
    lazy.layout.previous(),
    desc="Move focus up"),
  
  Key([mod], "Tab",
    lazy.layout.next(),
    desc="Move window focus to other window"),
  
  Key([mod, "shift"], "h",
    lazy.layout.shuffle_left(),
    desc="Move window to the left"),
  
  Key([mod, "shift"], "l",
    lazy.layout.shuffle_right(),
    desc="Move window to the right"),
 
  Key([mod, "shift"], "j",
    lazy.layout.shuffle_down(),
    desc="Move window down"),
 
  Key([mod, "shift"], "k",
    lazy.layout.shuffle_up(),
    desc="Move window up"),
  
  Key([mod], "n",
    lazy.layout.normalize(),
    desc="Reset all window sizes"),

  Key([mod, "shift"], "Return",
    lazy.layout.toggle_split(),
    desc="Toggle between split and unsplit sides of stack"),
  
  Key([mod],"Return",     
    lazy.spawn("terminal"),
    desc="Launch terminal"),
  
  Key([mod],"p",
    lazy.spawn("launcher"),
    desc="Run launcher"),
  
  Key([mod,"shift"],"b",
    lazy.spawn("browser"),
    desc="Launch browser"),
  
  Key([mod],"e",
    lazy.spawn("editwmconfig"),
    desc="Edit config.py"),

  Key([mod],"space",
    lazy.next_layout(),
    desc="Toggle between layouts"),
  
  Key([mod,"shift"],"c",  
    lazy.window.kill(),
    desc="Kill focused window"),
  
  Key([mod,"shift"],"r",
    lazy.reload_config(),
    desc="Reload the config"),
  
  Key([mod,"shift"],"q",
    lazy.shutdown(),
    desc="Shutdown Qtile"),
]

keys.extend(
  [
    Key(
      [mod],
      i.name,
      lazy.group[i.name].toscreen(),
      desc="Switch to group {}".format(i.name),),
    Key(
      [mod, "shift"],
      i.name,
      lazy.window.togroup(i.name, switch_group=True),
      desc="Switch to & move focused window to group {}".format(i.name),),
  ]
)

from libqtile.dgroups import simple_key_binder
dgroups_key_binder = simple_key_binder("mod4")

layout_theme = { "border_width":  2,
                 "margin":        5,
                 "border_focus":  "6AA1FD",
                 "border_normal": "555753", }

layouts = [
  layout.MonadTall(**layout_theme),
  layout.Max(**layout_theme),
  # MonadWide(),RatioTile(),Tile(),
  # TreeTab(),VerticalTile(),Zoomy(),
  # Stack(num_stacks=2),Bsp(),Matrix(),
  # Columns(border_focus_stack=["#d75f5f", "#8f3d3d"], border_width=4),
]

widget_defaults = dict(
  font="Play",
  fontsize=16,
  padding=10,
)

extension_defaults = widget_defaults.copy()

screens = [
  Screen(
    top=bar.Bar(
      [ 
        widget.GroupBox(),
        widget.WindowName(),
        widget.Chord(
          chords_colors={ "launch": ("#ff0000", "#ffffff"), },
          name_transform=lambda name: name.upper(),
        ),
        widget.Systray(),
        widget.CurrentLayout(),
        widget.Clock(format=" %D %I:%M %p "),
      ],
      30,
      border_width=[1, 1, 1, 1],  # Draw top and bottom borders
      border_color=["ff00ff", "ff00ff", "ff00ff", "ff00ff"]  # Borders are magenta
    ),
  ),
]

# Drag floating layouts.
mouse = [
  Drag([mod], "Button1", lazy.window.set_position_floating(), start=lazy.window.get_position()),
  Drag([mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()),
  Click([mod], "Button2", lazy.window.bring_to_front()),
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: list
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(
  float_rules=[
    *layout.Floating.default_float_rules, # Run the utility of `xprop` to see the wm class and name of an X client.
    Match(wm_class="confirmreset"),       # gitk
    Match(wm_class="makebranch"),         # gitk
    Match(wm_class="maketag"),            # gitk
    Match(wm_class="ssh-askpass"),        # ssh-askpass
    Match(title="branchdialog"),          # gitk
    Match(title="pinentry"),              # GPG key password entry
  ]
)

auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True
auto_minimize = True
wl_input_rules = None

@hook.subscribe.startup_once
def start_once():
  home = os.path.expanduser('~')
  subprocess.call([home + '/.config/qtile/autostart.lua'])

wmname = "qtile"
